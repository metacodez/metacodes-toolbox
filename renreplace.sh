# /////////////////////////////////////////////////////////////////////////////
# REFCODES.ORG
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being coveFG_RED by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////
#!/bin/bash

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

CURRENT_PATH="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_PATH}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"
MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f3- )"
if [ -z "${MODULE_NAME}" ]; then
	MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f2- )"
	if [ -z "${MODULE_NAME}" ]; then
		MODULE_NAME="${SCRIPT_DIR}"
	fi
fi
if [ -z ${COLUMNS} ] ; then
	export COLUMNS=$(tput cols)
fi

# ------------------------------------------------------------------------------
# ANSI ESCAPE CODES:
# ------------------------------------------------------------------------------

ESC_BOLD="\E[1m"
ESC_FAINT="\E[2m"
ESC_ITALIC="\E[3m"
ESC_UNDERLINE="\E[4m"
ESC_FG_RED="\E[31m"
ESC_FG_GREEN="\E[32m"
ESC_FG_YELLOW="\E[33m"
ESC_FG_BLUE="\E[34m"
ESC_FG_MAGENTA="\E[35m"
ESC_FG_CYAN="\E[36m"
ESC_FG_WHITE="\E[37m"
ESC_RESET="\E[0m"

# ------------------------------------------------------------------------------
# PRINTLN:
# ------------------------------------------------------------------------------

function printLn {
	char="-"
	if [[ $# == 1 ]] ; then
		char="$1"
	fi
	echo -en "${ESC_FAINT}"
	for (( i=0; i< ${COLUMNS}; i++ )) ; do
		echo -en "${char}"
	done
	echo -e "${ESC_RESET}"
}

# ------------------------------------------------------------------------------
# QUIT:
# ------------------------------------------------------------------------------

function quit {
	input=""
	while ([ "$input" != "q" ] && [ "$input" != "y" ]); do
		echo -ne "> Continue? Enter [${ESC_BOLD}q${ESC_RESET}] to quit, [${ESC_BOLD}y${ESC_RESET}] to continue: ";
		read input;
	done
	# printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
	if [ "$input" == "q" ] ; then
		printLn
		echo -e "> ${ESC_BOLD}Aborting due to user input.${ESC_RESET}"
		exit
	fi
}

# ------------------------------------------------------------------------------
# BANNER:
# ------------------------------------------------------------------------------

function printBanner {
	banner=$( figlet -w 999 "/${MODULE_NAME}:>>>${SCRIPT_NAME}..." 2> /dev/null )
	if [ $? -eq 0 ]; then
		echo "${banner}" | cut -c -${COLUMNS}
	else
		banner "${SCRIPT_NAME}..." 2> /dev/null
		if [ $? -ne 0 ]; then
			echo -e "> ${SCRIPT_NAME}:" | tr a-z A-Z 
		fi
	fi
}

printBanner

# ------------------------------------------------------------------------------
# HELP:
# ------------------------------------------------------------------------------

function printHelp {
	printLn
	echo -e "Usage: ${ESC_BOLD}${SCRIPT_NAME}${ESC_RESET}.sh <path> -f <find> -r <replace> | -h"
	printLn
	echo -e "Renames all folders recursively with name <find> to name <replace> starting at folder <path> and replaces all occurences of the text <find> with the text <replace> recursively in all files below the folder <path>."
	printLn
	echo -e "${ESC_BOLD}      <path>${ESC_RESET}: The path from which to recursively start finding and replacing"
	echo -e "${ESC_BOLD}   -f <find>${ESC_RESET}: The \"find\" text for folders and text to be renamed from"
	echo -e "${ESC_BOLD}-r <replace>${ESC_RESET}: The \"replace\" text for folders and text to be renamed to"
	echo -e "${ESC_BOLD}          -v${ESC_RESET}: Be more verbose"
	echo -e "${ESC_BOLD}          -h${ESC_RESET}: Print this help"
	printLn
}

# ------------------------------------------------------------------------------
# ARGS:
# ------------------------------------------------------------------------------

POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
	case $1 in
		-h|--help)
			printHelp
			exit 0
			;;
		-v|--verbose) 
			verbose="1"
			shift # Skip $1
			# shift # Skip $2 if $1 has an argument
			;;
		-f|--find) 
			find="$2"
			shift # Skip $1
			shift # Skip $2 being argument of $1
			;;
		-r|--replace) 
			replace="$2"
			shift # Skip $1
			shift # Skip $2 being argument of $1
			;;
		-*|--*)
			printHelp
			echo -e "> Unknown argument <${ESC_BOLD}${ESC_FG_RED}$1${ESC_RESET}>"
			exit 1
			;;
		*)
			POSITIONAL_ARGS+=("$1") # Save positional arg
			shift # Past argument
			;;
	esac
done
set -- "${POSITIONAL_ARGS[@]}"

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------

if [ "$#" -ne 1 ]; then
	printHelp
    echo "Expected one <path> argument for the folder from which to start!"
    exit 1
fi

if [ -z "$find" ]; then
	printHelp
    echo "Expected a <find> argument for the text to search for!"
    exit 1
fi

if [ -z "$replace" ]; then
	printHelp
    echo "Expected a <replace> argument for the text to replace with!"
    exit 1
fi

path="$1"

if [[ "$verbose" == "1" ]]; then
	printLn
	echo "> Path = \"$path\""
	echo "> Find = \"$find\""
	echo "> Replace = \"$replace\""
	printLn
	quit
	printLn
fi

find "$path" -type d -name "$find" | sort -r | while read dir; do
    parent_dir=$(dirname "$dir")
    new_dir="$parent_dir/$replace"
    if [[ "$verbose" == "1" ]]; then
    	echo "> Renaming folder <$dir> to <"$new_dir"> ..."
    fi
    mv "$dir" "$new_dir"
done

if [[ "$verbose" == "1" ]]; then
	echo "> Replacing text \"$find\" to \"$replace\" ..."
fi
find "$path" -type f -exec sed -i "s/$find/$replace/g" {} +
if [[ "$verbose" == "1" ]]; then
	echo "> Done."
fi
