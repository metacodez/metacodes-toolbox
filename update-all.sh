# /////////////////////////////////////////////////////////////////////////////
# REFCODES.ORG
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being covered by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////
#!/bin/bash

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

CURRENT_PATH="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_PATH}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"
MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f3- )"
if [ -z "${MODULE_NAME}" ]; then
	MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f2- )"
	if [ -z "${MODULE_NAME}" ]; then
		MODULE_NAME="${SCRIPT_DIR}"
	fi
fi
if [ -z ${COLUMNS} ] ; then
	export COLUMNS=$(tput cols)
fi

# ------------------------------------------------------------------------------
# ANSI ESCAPE CODES:
# ------------------------------------------------------------------------------

ESC_BOLD="\E[1m"
ESC_FAINT="\E[2m"
ESC_ITALIC="\E[3m"
ESC_UNDERLINE="\E[4m"
ESC_FG_RED="\E[31m"
ESC_FG_GREEN="\E[32m"
ESC_FG_YELLOW="\E[33m"
ESC_FG_BLUE="\E[34m"
ESC_FG_MAGENTA="\E[35m"
ESC_FG_CYAN="\E[36m"
ESC_FG_WHITE="\E[37m"
ESC_RESET="\E[0m"

# ------------------------------------------------------------------------------
# PRINTLN:
# ------------------------------------------------------------------------------

function printLn {
	char="-"
	if [[ $# == 1 ]] ; then
		char="$1"
	fi
	echo -en "${ESC_FAINT}"
	for (( i=0; i< ${COLUMNS}; i++ )) ; do
		echo -en "${char}"
	done
	echo -e "${ESC_RESET}"
}
# ------------------------------------------------------------------------------
# QUIT:
# ------------------------------------------------------------------------------

function quit {
	input=""
	while ([ "$input" != "q" ] && [ "$input" != "y" ]); do
		echo -ne "> Continue? Enter [${ESC_BOLD}q${ESC_RESET}] to quit, [${ESC_BOLD}y${ESC_RESET}] to continue: ";
		read input;
	done
	# printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
	if [ "$input" == "q" ] ; then
		printLn
		echo -e "> ${ESC_BOLD}Aborting due to user input.${ESC_RESET}"
		cd "${CURRENT_PATH}"
		exit 1
	fi
}

# ------------------------------------------------------------------------------
# BANNER:
# ------------------------------------------------------------------------------

function printBanner {
	banner=$( figlet -w 999 "/${MODULE_NAME}:>>>${SCRIPT_NAME}..." 2> /dev/null )
	if [ $? -eq 0 ]; then
		echo "${banner}" | cut -c -${COLUMNS}
	else
		banner "${SCRIPT_NAME}..." 2> /dev/null
		if [ $? -ne 0 ]; then
			echo -e "> ${SCRIPT_NAME}:" | tr a-z A-Z 
		fi
	fi
}

printBanner

# ------------------------------------------------------------------------------
# HELP:
# ------------------------------------------------------------------------------

function printHelp {
	printLn
	echo -e "Usage: ${ESC_BOLD}${SCRIPT_NAME}${ESC_RESET}.sh -h | <artifactVertsion> <releaseVersion>"
	printLn
	echo -e "Updates the versions found in applicable resources for all artifacts from the <artifactVertsion> to the <releaseVersion>."
	printLn
	echo -e "${ESC_BOLD}                -h${ESC_RESET}: Print this help"
	echo -e "${ESC_BOLD}<artifactVertsion>${ESC_RESET}: The version from which to upgrade all resources."
	echo -e "${ESC_BOLD}  <releaseVersion>${ESC_RESET}: The version to which to upgrade all resources."
	printLn
}

if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
	printHelp
	exit 0
fi

# ------------------------------------------------------------------------------
# FUNCTIONS:
# ------------------------------------------------------------------------------

function updateAll {
	echo -e "> Updating <$(pwd)> ..."
	# Markdown Javadoc Link-Pattern:
	artifactJavadoc="\/${artifactVersion}\/org\/refcodes"
	releaseJavadoc="\/${releaseVersion}\/org\/refcodes"
	echo -e "> Replacing: ${ESC_FAINT}${artifactJavadoc}${ESC_RESET}"
	echo -e ">      With: ${ESC_BOLD}${releaseJavadoc}${ESC_RESET}"
	find . -type f \( -name "*.md" -or -name "*.java" -or -name "*.txt" -or -name "*.xml" -or -name "*.html" \) ! -name "changelist*" ! -name "*change_list*" -exec sed -i "s/${artifactJavadoc}/${releaseJavadoc}/g" {} \;

	# Maven Dependency, case a):
	artifactDependency="<groupId>org.refcodes<\/groupId>"'\n\t\t'"<version>${artifactVersion}<\/version>"'\n\t'"<\/dependency>"
	releaseDependency="<groupId>org.refcodes<\/groupId>"'\n\t\t'"<version>${releaseVersion}<\/version>"'\n\t'"<\/dependency>"
	echo -e "> Replacing: ${ESC_FAINT}${artifactDependency}${ESC_RESET}"
	echo -e ">      With: ${ESC_BOLD}${releaseDependency}${ESC_RESET}"
	find . -type f \( -name "*.md" -or -name "*.java" -or -name "*.txt" -or -name "*.xml" -or -name "*.html" \) ! -name "changelist*" ! -name "*change_list*" -exec sed -i ':a;N;$!ba;'"s/${artifactDependency}/${releaseDependency}/g" {} \;

	# Maven Dependency, case b):
	artifactDependency="<\/artifactId>"'\n\t\t'"<version>${artifactVersion}<\/version>"'\n\t'"<\/dependency>"
	releaseDependency="<\/artifactId>"'\n\t\t'"<version>${releaseVersion}<\/version>"'\n\t'"<\/dependency>"
	echo -e "> Replacing: ${ESC_FAINT}${artifactDependency}${ESC_RESET}"
	echo -e ">      With: ${ESC_BOLD}${releaseDependency}${ESC_RESET}"
	find . -type f \( -name "*.md" -or -name "*.java" -or -name "*.txt" -or -name "*.xml" -or -name "*.html" \) ! -name "changelist*" ! -name "*change_list*" -exec sed -i ':a;N;$!ba;'"s/${artifactDependency}/${releaseDependency}/g" {} \;

	# Maven Dependency, case c):
	artifactDependency="-DarchetypeVersion=${artifactVersion}"
	releaseDependency="-DarchetypeVersion=${releaseVersion}"
	echo -e "> Replacing: ${ESC_FAINT}${artifactDependency}${ESC_RESET}"
	echo -e ">      With: ${ESC_BOLD}${releaseDependency}${ESC_RESET}"
	find . -type f \( -name "*.md" \) ! -name "changelist*" ! -name "*change_list*" -exec sed -i ':a;N;$!ba;'"s/${artifactDependency}/${releaseDependency}/g" {} \;

	# pom.xml - SNAPSHOTS, case a):
	artifactDependency="<groupId>org.refcodes<\/groupId>"'\n\t\t'"<version>${artifactVersion}-SNAPSHOT<\/version>"'\n\t'"<\/dependency>"
	releaseDependency="<groupId>org.refcodes<\/groupId>"'\n\t\t'"<version>${releaseVersion}<\/version>"'\n\t'"<\/dependency>"
	echo -e "> Replacing: ${ESC_FAINT}${artifactDependency}${ESC_RESET}"
	echo -e ">      With: ${ESC_BOLD}${releaseDependency}${ESC_RESET}"
	find . -type f -name "pom.xml" -exec sed -i ':a;N;$!ba;'"s/${artifactDependency}/${releaseDependency}/g" {} \;

	# pom.xml - SNAPSHOTS, case b):
	artifactDependency="<\/artifactId>"'\n\t\t'"<version>${releaseVersion}-SNAPSHOT<\/version>"'\n\t'"<\/dependency>"
	releaseDependency="<\/artifactId>"'\n\t\t'"<version>${releaseVersion}<\/version>"'\n\t'"<\/dependency>"
	echo -e "> Replacing: ${ESC_FAINT}${artifactDependency}${ESC_RESET}"
	echo -e ">      With: ${ESC_BOLD}${releaseDependency}${ESC_RESET}"
	find . -type f -name "pom.xml" -exec sed -i ':a;N;$!ba;'"s/${artifactDependency}/${releaseDependency}/g" {} \;

	# pom.xml - SNAPSHOTS, case c): refcodes-meta
	artifactDependency="<groupId>org.refcodes<\/groupId>"'\n\t'"<version>${artifactVersion}-SNAPSHOT<\/version>"'\n\t'"<\/dependency>"
	releaseDependency="<groupId>org.refcodes<\/groupId>"'\n\t'"<version>${releaseVersion}-SNAPSHOT<\/version>"'\n\t'"<\/dependency>"
	echo -e "> Replacing: ${ESC_FAINT}${artifactDependency}${ESC_RESET}"
	echo -e ">      With: ${ESC_BOLD}${releaseDependency}${ESC_RESET}"
	find . -type f -name "pom.xml" -exec sed -i ':a;N;$!ba;'"s/${artifactDependency}/${releaseDependency}/g" {} \;
}

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------

CONF_PATH="${SCRIPT_PATH}/meta.conf"
if [ -f "${CONF_PATH}" ]; then
  . ${CONF_PATH}
fi
# Update project references:
artifactVersion="$1"
releaseVersion="$2"
if [ -z "${artifactVersion}" ] ; then
	echo -e "> Usage: ${SCRIPT_NAME} <artifactVertsion> <releaseVersion>" 1>&2;
	echo -e "> A (previous) artifact version is required as first argument (e.g. \"1.0.2\"). Aborting!" 1>&2;
	exit 1
fi
if [ -z "${releaseVersion}" ] ; then
	echo -e "> Usage: ${SCRIPT_NAME} <artifactVertsion> <releaseVersion>" 1>&2;
	echo -e "> A release artifact version is required as first argument (e.g. \"1.0.3\"). Aborting!" 1>&2;
	exit 1
fi
echo -e "> Updating version <${ESC_FAINT}${artifactVersion}${ESC_RESET}> to version <${ESC_BOLD}${releaseVersion}${ESC_RESET}>..."

if [ -d "${BLOG_PATH}" ]; then
    cd "${BLOG_PATH}"
	updateAll
fi

cd "${PARENT_PATH}"
updateAll

cd "${CURRENT_PATH}"