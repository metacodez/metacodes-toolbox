# /////////////////////////////////////////////////////////////////////////////
# REFCODES.ORG
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being covered by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////

# /////////////////////////////////////////////////////////////////////////////
# This script tries to release all artifacts in the order as specified in  the 
# file "${DEPLOY_REACTOR}". Make sure you updated this file in case your maven 
# #dependencies of the top level artifacts changed! In case you want to run a
# meaningful dry run, please remove all artifacts from your home folder's 
# "~/.m2/repository" directory! Only then you can observe whether the 
# dependencies of the "${DEPLOY_REACTOR}" are in the right order and complete!   
# /////////////////////////////////////////////////////////////////////////////
#!/bin/bash

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

MAVEN_HELP_PLUGIN_VERSION=3.4.0

CURRENT_PATH="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_PATH}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"
MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f3- )"
if [ -z "${MODULE_NAME}" ]; then
	MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f2- )"
	if [ -z "${MODULE_NAME}" ]; then
		MODULE_NAME="${SCRIPT_DIR}"
	fi
fi
if [ -z ${COLUMNS} ] ; then
	export COLUMNS=$(tput cols)
fi

# ------------------------------------------------------------------------------
# ANSI ESCAPE CODES:
# ------------------------------------------------------------------------------

ESC_BOLD="\E[1m"
ESC_FAINT="\E[2m"
ESC_ITALIC="\E[3m"
ESC_UNDERLINE="\E[4m"
ESC_FG_RED="\E[31m"
ESC_FG_GREEN="\E[32m"
ESC_FG_YELLOW="\E[33m"
ESC_FG_BLUE="\E[34m"
ESC_FG_MAGENTA="\E[35m"
ESC_FG_CYAN="\E[36m"
ESC_FG_WHITE="\E[37m"
ESC_RESET="\E[0m"

# ------------------------------------------------------------------------------
# PRINTLN:
# ------------------------------------------------------------------------------

function printLn {
	char="-"
	if [[ $# == 1 ]] ; then
		char="$1"
	fi
	echo -en "${ESC_FAINT}"
	for (( i=0; i< ${COLUMNS}; i++ )) ; do
		echo -en "${char}"
	done
	echo -e "${ESC_RESET}"
}
# ------------------------------------------------------------------------------
# QUIT:
# ------------------------------------------------------------------------------

function quit {
	input=""
	while ([ "$input" != "q" ] && [ "$input" != "y" ]); do
		echo -ne "> Continue? Enter [${ESC_BOLD}q${ESC_RESET}] to quit, [${ESC_BOLD}y${ESC_RESET}] to continue: ";
		read input;
	done
	if [ "$input" == "q" ] ; then
		printLn
		echo -e "> ${ESC_BOLD}Aborting due to user input.${ESC_RESET}"
		cd "${CURRENT_PATH}"
		exit 1
	fi
}

# ------------------------------------------------------------------------------
# BANNER:
# ------------------------------------------------------------------------------

function printBanner {
	banner=$( figlet -w 999 "/${MODULE_NAME}:>>>${SCRIPT_NAME}..." 2> /dev/null )
	if [ $? -eq 0 ]; then
		echo "${banner}" | cut -c -${COLUMNS}
	else
		banner "${SCRIPT_NAME}..." 2> /dev/null
		if [ $? -ne 0 ]; then
			echo -e "> ${SCRIPT_NAME}:" | tr a-z A-Z 
		fi
	fi
}

printBanner

# ------------------------------------------------------------------------------
# HELP:
# ------------------------------------------------------------------------------

function printHelp {
	printLn
	echo -e "Usage: ${ESC_BOLD}${SCRIPT_NAME}${ESC_RESET}.sh -h"
	printLn
	echo -e "Releases all projects found in the parent folder of the script's directory as of the <reactor.txt> file to Maven-Central."
	printLn
	echo -e "${ESC_BOLD}-h${ESC_RESET}: Print this help"
	printLn
}

if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
	printHelp
	exit 0
fi

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------

NOW="$(date +%Y-%m-%dT%H.%M.%S)"
LOG_PATH="${SCRIPT_PATH}/${SCRIPT_NAME}-${NOW}.log"
USER="$(whoami)"
echo -e "#!/usr/bin/bash" >> ${LOG_PATH}
echo -e "# Invoked at ${NOW} by <${USER}> ..." >> ${LOG_PATH}
trap 'echo -e "$BASH_COMMAND" >> ${LOG_PATH}' DEBUG

# ------------------------------------------------------------------------------
# FUNCTIONS:
# ------------------------------------------------------------------------------

function askQuitContinue {
	input=""
	while ([ "$input" !=  "q" ] && [ "$input" !=  "y" ] ); do
		echo -ne "> Continue? Enter [${ESC_BOLD}q${ESC_RESET}] to quit, [${ESC_BOLD}y${ESC_RESET}] to continue: "; read input;
	done
	printLn
	if [ "$input" == "q" ] ; then
		echo -e "> ${ESC_FG_RED}Aborting due to user input.${ESC_RESET}"
		exit 1
	fi
}

function askQuitContinueDryRun {
	isDryRun="n"
	input=""
	while ([ "$input" !=  "q" ] && [ "$input" !=  "y" ] && [ "$input" !=  "d" ] ); do
		echo -ne "> Continue? Enter [${ESC_BOLD}q${ESC_RESET}] to quit, [${ESC_BOLD}y${ESC_RESET}] or [${ESC_BOLD}d${ESC_RESET}] for a dry-run: "; read input;
	done
	printLn
	if [ "$input" == "q" ] ; then
		echo -e "> ${ESC_FG_RED}Aborting due to user input.${ESC_RESET}"
		exit 1
	fi
	if [ "$input" == "d" ] ; then
		isDryRun="y"
		echo -e "> Continuing in dry-run mode ( isDryRun = \"${isDryRun}\" )..."
	fi
}

# ------------------------------------------------------------------------------
# BEGIN:
# ------------------------------------------------------------------------------

unset JAVA_TOOL_OPTIONS
cd ${SCRIPT_PATH}
report=""

# ------------------------------------------------------------------------------
# Show information on release:
# ------------------------------------------------------------------------------

DEPLOY_REACTOR="reactor.txt"
currentArtifact="${CURRENT_PATH##*/}"
parentArtifact="${currentArtifact/meta/parent}"
vendor="${currentArtifact/-meta/}"
projectPath=${PARENT_PATH}
echo -e "> ${ESC_BOLD}Deploying ${vendor} \"release\" artifacts ...${ESC_RESET}"
printLn
java -version
printLn
echo -e "Identified vendor        = \"$vendor\""
echo -e "Using meta-artifact      = \"$currentArtifact\""
echo -e "Using parent-artifact    = \"$parentArtifact\""
echo -e "Using deploy reactor     = \"$DEPLOY_REACTOR\""
echo -e "Project path             = \"$projectPath\""
printLn

# ------------------------------------------------------------------------------
# Determine mode for continuing:
# ------------------------------------------------------------------------------

askQuitContinueDryRun
if [[ "${isDryRun}" == "n" ]]; then
	ssh -T git@bitbucket.org > /dev/null
	if (($? > 0)); then
		git config --global credential.helper cache
	fi
fi

# ------------------------------------------------------------------------------
# Preconditions:
# ------------------------------------------------------------------------------

printLn
echo -e "> ${ESC_BOLD}Testing artifacts preconditions ...${ESC_RESET}"
while read artifact; do
	if [[ $artifact != \#* ]] ; then
		artifactPath="${SCRIPT_PATH}/../${artifact}"
		pomFile="${artifactPath}/pom.xml"
		isDir="n"
		hasPom="n"
		hasPomParentVersion="n"
		hasPomArtifactVersion="n"
		isDirAnsi="${ESC_BOLD}${ESC_FG_RED}n${ESC_RESET}"
		hasPomAnsi="${ESC_BOLD}${ESC_FG_RED}n${ESC_RESET}"
		hasPomParentVersionAnsi="${ESC_BOLD}${ESC_FG_RED}n${ESC_RESET}"
		hasPomArtifactVersionAnsi="${ESC_BOLD}${ESC_FG_RED}n${ESC_RESET}"
		
		if [ -d "${artifactPath}" ]; then
			isDir="y"
			isDirAnsi="${ESC_BOLD}${ESC_FG_GREEN}y${ESC_RESET}"
			if [ -f "${pomFile}" ]; then
				hasPom="y"
				hasPomAnsi="${ESC_BOLD}${ESC_FG_GREEN}y${ESC_RESET}"
				cat "${pomFile}" | xml2 | grep "^/project/parent/version=" >/dev/null
				if (($? == 0)); then
					hasPomParentVersion="y"
					hasPomParentVersionAnsi="${ESC_BOLD}${ESC_FG_GREEN}y${ESC_RESET}"
				fi
				cat "${pomFile}" | xml2 | grep "^/project/version=" >/dev/null
				if (($? == 0)); then
					hasPomArtifactVersion="y"
					hasPomArtifactVersionAnsi="${ESC_BOLD}${ESC_FG_GREEN}y${ESC_RESET}"
				fi
			fi
		fi
		
		abort="n"
		precondition="${ESC_BOLD}${ESC_FG_GREEN}SUCCESS${ESC_RESET}"
		if [[ "${hasPomArtifactVersion}" == "n" ]] && [[ "${hasPomParentVersion}" == "n" ]] ; then
			precondition="${ESC_BOLD}${ESC_FG_RED}FAIL${ESC_RESET}"
			abort="y"
		fi
		printf "[\033[1;33m%-32s${ESC_RESET}]: Directory = [${isDirAnsi}], POM = [${hasPomAnsi}], Parent version = [${hasPomParentVersionAnsi}], Artifact version = [${hasPomArtifactVersionAnsi}] --> [${precondition}]\n" "${artifact}"
	fi
done < "${SCRIPT_PATH}/${DEPLOY_REACTOR}"
printLn
if [[ "${abort}" == "y" ]] ; then
	echo -e 2> "> ${ESC_FG_RED}Aborting due to failed verification of the preconditions.${ESC_RESET}"
	exit 1
fi
echo -e "> PLEASE *VALIDATE* THE ${ESC_BOLD}ARCHTYPES${ESC_RESET} AND VERYFY *BOTH* ${ESC_BOLD}REFCODES VERSIONS${ESC_RESET} IN THE PARENT-POM!"
printLn
askQuitContinue

# ------------------------------------------------------------------------------
# LOOP through all artifacts:
# ------------------------------------------------------------------------------

while read artifact; do
	if [[ $artifact != \#* ]] ; then
		if [ ! -z "${artifact}" ]; then 
			artifactPath="${SCRIPT_PATH}/../${artifact}"
			cd "${artifactPath}"
			
			# ------------------------------------------------------------------
			# Next artifact to go:
			# ------------------------------------------------------------------
	
			pomFile="${artifactPath}/pom.xml"
			echo -e "> ${ESC_BOLD}Processing artifact \"${artifact}\" in folder \"${artifactPath}\" ...${ESC_RESET}"
			
			# ------------------------------------------------------------------
			# Determine artifact meta data:
			# ------------------------------------------------------------------
	
			artifactVersion=$(mvn org.apache.maven.plugins:maven-help-plugin:${MAVEN_HELP_PLUGIN_VERSION}:evaluate -Dexpression=project.version 2>/dev/null | grep -E -v '^\[|Download' | grep -E -v 'WARNING' | grep -E -v 'INFO' | tr -d ' \n')
			releaseVersion=${artifactVersion/-SNAPSHOT/}
			nextVersion="$(echo ${releaseVersion} | awk -F. -v OFS=. 'NF==1{print ++$NF}; NF>1{if(length($NF+1)>length($NF))$(NF-1)++; $NF=sprintf("%0*d", length($NF), ($NF+1)%(10^length($NF))); print}')-SNAPSHOT"
			printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' '-'
			echo -e "Current artifact ID      = \"${artifact}\""
			echo -e "Artifact version         = \"${artifactVersion}\""
			if ( [[ ${artifactVersion} != *-SNAPSHOT* ]] ) ; then
				isRelease="n"
			else
				isRelease="y"
			fi
			echo -e "Release version          = \"${releaseVersion}\""
			echo -e "Next version             = \"${nextVersion}\""
			echo -e "Release artifact?        = \"${isRelease}\""
			
			# ------------------------------------------------------------------
			# Which versions to update? Parent or artifact?:
			# ------------------------------------------------------------------
	
			hasPomParentVersion="n"
			cat "${pomFile}" | xml2 | grep "^/project/parent/version=" >/dev/null
			if (($? == 0)); then
				hasPomParentVersion="y"
				echo -e "POM parent version found = \"${hasPomParentVersion}\""
			fi
			hasPomArtifactVersion="n"
			cat "${pomFile}" | xml2 | grep "^/project/version=" >/dev/null
			if (($? == 0)); then
				hasPomArtifactVersion="y"
				echo -e "POM version found        = \"${hasPomArtifactVersion}\""
			fi
			if [[ "${hasPomArtifactVersion}" == "n" ]] && [[ "${hasPomParentVersion}" == "n" ]] ; then
				echo -e "> ${ESC_BOLD}Warning: Artifact \"${artifact}\" has neither parent POM nor artifact version declaration! ...${ESC_RESET}"
			fi
			
			printLn
			
			# ------------------------------------------------------------------
			# RELEASE:
			# ------------------------------------------------------------------
	
			if [[ "${isRelease}" == "y" ]] && [[ "${isDryRun}" == "n" ]]; then
				echo -e "> Transforming all SNAPSHOT versions of \"${artifact}\" to \"release\" versions ..."
				
				# --------------------------------------------------------------
				# Remove all "-SNAPSHOT" suffixes fromm all versions:
				# --------------------------------------------------------------
	
				find . -type f -name "pom.xml" -exec sed -i 's/-SNAPSHOT//g' {} \;
				
				# --------------------------------------------------------------		
				# Commit and push RELEASE changes before releasing:
				# --------------------------------------------------------------
	
				echo -e "> Committing and pushing RELEASE artifact \"${artifact}\" to GIT ..."
				git commit -a -m "[$SCRIPT_NAME]: Preparing release ${releaseVersion} of artifact ${artifact} with version ${artifactVersion}"
				git push origin master
				
				# --------------------------------------------------------------
				# Deploy to maven staging repo:
				# --------------------------------------------------------------

				echo -e "> Doing a maven deploy on \"${artifact}\" ..."
				result=$(mvn deploy -P release)
				code=$?
				if (($code != 0)); then
					wtf="A message body reader for Java class com.sonatype.nexus.staging.api.dto.StagingProfileRepositoryDTO, and Java type class com.sonatype.nexus.staging.api.dto.StagingProfileRepositoryDTO, and MIME media type text/html was not found"
					if [[ "$result" == *"$wtf"* ]] ; then
						# Upon deploy WTF issue::
						echo -e "> ${ESC_FG_CYAN}Ecnountered ignorable failure deploying artifact \"${artifact}\" with failure code <$code> :-/${ESC_RESET} (ignored)"
					else
						# Upon deploy failure:
						if [ ! -z "${report}" ] ; then
							report="${report}"
						fi
						report="${report}   [FAIL]: ${ESC_FG_RED}${artifact}${ESC_RESET}\n"
						echo -e "${result}"
						echo -e "> ${ESC_FG_RED}Failed deploying artifact \"${artifact}\" :-(${ESC_RESET}"
						exit 1
					fi
				fi
				# Upon deploy success:
				tag="${artifact}-${releaseVersion}"
				echo -e "> Creating tag \"${tag}\" ..."
				# Create a GIT release TAG:
				git tag -a "${tag}" -m "[$SCRIPT_NAME]: Release ${releaseVersion} of artifact ${artifact} (${tag})"
				git push --tags
				echo -e "> Updating artifact version to ${nextVersion} ..."
				
				# ----------------------------------------------------------
				# Set next artifact SNAPSHOT version:
				# ----------------------------------------------------------

				if [[ "${hasPomParentVersion}" == "y" ]] ; then
					# UPDATE POM's PARENT VERSION:
					result=$(mvn versions:update-parent -DparentVersion=${nextVersion} -DallowS­nap­shots=true)
					if (($? != 0)); then
						# Upon failure:
						echo -e "${result}"
						echo -e "> ${ESC_FG_RED}Failed to update POM's parent version to ${nextVersion}.${ESC_RESET}"
						exit 1
					else
						result=$(mvn versions:commit)
						if (($? != 0)); then
							# Upon failure:
							echo -e "${result}"
							echo -e "> ${ESC_FG_CYAN}Failed to update to new version ${nextVersion} (ignored).${ESC_RESET}"
						fi
					fi
				fi
				if [[ "${hasPomArtifactVersion}" == "y" ]] ; then
					# UPDATE POM's ARTIFACT VERSION:
					result=$(mvn versions:set -DnewVersion=${nextVersion})
					if (($? != 0)); then
						# Upon failure:
						echo -e "${result}"
						echo -e "> ${ESC_FG_RED}Failed to update POM's version to ${nextVersion}.${ESC_RESET}"
						exit 1
					else
						result=$(mvn versions:commit)
						if (($? != 0)); then
							# Upon failure:
							echo -e "${result}"
							echo -e "> ${ESC_FG_CYAN}Failed to update to new version ${nextVersion} (ignored).${ESC_RESET}"
						fi
						
						# --------------------------------------------------
						# As this artifact may be a parent by itself, we 
						# install it for other artifact's parent versions to
						# be set to this artifaxt's SNAPSHOT version. If
						# this SNAPSHOT has not been installed, then then
						# other artifact's paren versions cannot not be set:
						# --------------------------------------------------

						result=$(mvn clean install)
						if (($? != 0)); then
							# Upon failure:
							echo -e "${result}"
							echo -e "> ${ESC_FG_RED}Failed to install artifact with POM's version ${nextVersion}.${ESC_RESET}"
							exit 1
						fi
					fi
				fi
					
				# ----------------------------------------------------------
				# Set artifact's parent SNAPSHOT version:
				# ----------------------------------------------------------

				releaseParent="<version>${releaseVersion}<\/version>"'\n\t'"<\/parent>"
				nextParent="<version>${nextVersion}<\/version>"'\n\t'"<\/parent>"
				find . -type f -name "pom.xml" -exec sed -i ':a;N;$!ba;'"s/${releaseParent}/${nextParent}/g" {} \;
				
				# ----------------------------------------------------------
				# Set archetype's SNAPSHOT version:
				# ----------------------------------------------------------

				releaseArchetype="<version>${releaseVersion}<\/version>"'\n\t\t'"<relativePath\/>"
				nextArchetype="<version>${nextVersion}<\/version>"'\n\t\t'"<relativePath\/>"
				find . -type f -name "pom.xml" -exec sed -i ':a;N;$!ba;'"s/${releaseArchetype}/${nextArchetype}/g" {} \;

				# ----------------------------------------------------------
				# Set artifact's REFCODES SNAPSHOT version:
				# ----------------------------------------------------------

				artifactRefcodes="<org.refcodes.version>${artifactVersion}<\/org.refcodes.version>"
				releaseRefcodes="<org.refcodes.version>${releaseVersion}<\/org.refcodes.version>"
				nextRefcodes="<org.refcodes.version>${nextVersion}<\/org.refcodes.version>"
				find . -type f -name "pom.xml" -exec sed -i "s/${artifactRefcodes}/${nextRefcodes}/g" {} \;
				find . -type f -name "pom.xml" -exec sed -i "s/${releaseRefcodes}/${nextRefcodes}/g" {} \;
				
				# ----------------------------------------------------------	
				# Commit and push next SNAPSHOT changes before releasing:
				# ----------------------------------------------------------

				echo -e "> Committing and pushing new SNAPSHOT artifact \"${artifact}\" to GIT ..."
				# Commit next SNAPSHOT version to GIT:
				git commit -a -m "[$SCRIPT_NAME]: Updated artifact ${artifact} to version ${nextVersion}"
				git push origin master
				if [ ! -z "${report}" ] ; then
					report="${report}"
	
				fi
				report="${report}[SUCCESS]: ${ESC_FG_GREEN}${artifact}${ESC_RESET}\n"
				echo -e "> ${ESC_FG_GREEN}Successfully deployed artifact \"${artifact}\" :-)${ESC_RESET}"
			fi
			
			# ------------------------------------------------------------------
			# DRY RUN:
			# ------------------------------------------------------------------
	
			if [[ "${isRelease}" == "y" ]] && [[ "${isDryRun}" == "y" ]]; then
				cd "${artifactPath}"
				result=$(mvn 'javadoc:javadoc' clean install)
				if (($? != 0)); then
					# Upon failure:
					if [ ! -z "${report}" ] ; then
						report="${report}"
							fi
					report="${report} [REJECT]: ${ESC_FG_RED}${artifact}${ESC_RESET}\n"
					echo -e "${result}"
					echo -e "> ${ESC_FG_RED}Failed deploying artifact \"${artifact}\" :-(${ESC_RESET}"
					exit 1
				else
					if [ ! -z "${report}" ] ; then
						report="${report}"
		
					fi
					report="${report}   [PASS]: ${ESC_FG_GREEN}${artifact}${ESC_RESET}\n"
					echo -e "> ${ESC_FG_GREEN}Successfully installed artifact \"${artifact}\" :-)${ESC_RESET}"
				fi
			fi
			
			# ------------------------------------------------------------------
			# SKIP:
			# ------------------------------------------------------------------
	
			if [[ "${isRelease}" == "n" ]] ; then
				if [ ! -z "${report}" ] ; then
					report="${report}"
		
				fi
				report="${report}[SKIPPED]: ${ESC_FG_YELLOW}${artifact}${ESC_RESET}\n"
				echo -e "> ${ESC_FG_GREEN}Skipping artifact \"${artifact}\"- no \"SNAPSHOT\" version ...${ESC_RESET}"
			fi
		fi
	fi
done < "${SCRIPT_PATH}/${DEPLOY_REACTOR}"

# ------------------------------------------------------------------------------
# Set META artifact's SNAPSHOT version:
# ------------------------------------------------------------------------------

cd  "${SCRIPT_PATH}"

artifactVersion=$(mvn org.apache.maven.plugins:maven-help-plugin:${MAVEN_HELP_PLUGIN_VERSION}:evaluate -Dexpression=project.version 2>/dev/null | grep -E -v '^\[|Download' | grep -E -v 'WARNING' | grep -E -v 'INFO' | tr -d ' \n')
releaseVersion=${artifactVersion/-SNAPSHOT/}
nextVersion="$(echo ${releaseVersion} | awk -F. -v OFS=. 'NF==1{print ++$NF}; NF>1{if(length($NF+1)>length($NF))$(NF-1)++; $NF=sprintf("%0*d", length($NF), ($NF+1)%(10^length($NF))); print}')-SNAPSHOT"
artifactMeta="<artifactId>refcodes-meta<\/artifactId>"'\n\t'"<version>${artifactVersion}<\/version>"
nextMeta="<artifactId>refcodes-meta<\/artifactId>"'\n\t'"<version>${nextVersion}<\/version>"

echo "artifactVersion=$artifactVersion"
echo "releaseVersion=$releaseVersion"
echo "nextVersion=$nextVersion"
echo "artifactMeta=$artifactMeta"
echo "nextMeta=$nextMeta"

if [[ "${isDryRun}" == "n" ]]; then
	sed -i ':a;N;$!ba;'"s/${artifactMeta}/${nextMeta}/g" pom.xml
fi

# ------------------------------------------------------------------------------
# Done:
# ------------------------------------------------------------------------------

printLn
echo -e "Deploy status:"
printLn
echo -e "${report}"
printLn
cd "${CURRENT_PATH}"
echo -e "*** Call \"./latest-releases.sh\" to verify deployed versions ***"
echo -e "*** Call \"./publish-all.sh ${releaseVersion}\" to update all your Maven Central artifact's Javadoc at javadoc.io ***"
echo -e "*** Call \"./changelist-all.sh <previousVersion> ${releaseVersion}\" to create a change list ***"
echo -e "*** Call \"./update-all.sh <previousVersion> ${releaseVersion}\" to update all project's documentation references ***"

# echo -e "> Done."
