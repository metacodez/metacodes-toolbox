# README #

This repository represents a toolbox of scripts used multiple times throughout
the [`METACODES.PRO`](https://www.metacodes.pro) projects though which do not 
have an owner project so this `toolbox` repository becomes the owner. 

> This repository therewith contains the master files of thos redundant files found in the according projects.

Use `bulk-updater.sh <srcDir> <destDir>` to update the destination folder's files 
recursively with the master files found in the source foilder (excluding this "Readme.md"
file).

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/metacodez/metacodes-toolbox/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

