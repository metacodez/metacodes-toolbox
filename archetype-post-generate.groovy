import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

Path gitIgnorePath = Paths.get(request.outputDirectory, request.artifactId, "gitignore");
if (Files.exists(gitIgnorePath)) {
    Path targetPath = gitIgnorePath.getParent().resolve(".gitignore");
    try {
        Files.move(gitIgnorePath, targetPath);
    } catch (Exception e) {
        System.err.println("Encountered a <" + e.getClass().getName() + "> exception when trying to post processing the <gitignore> file to <.gitignore>: " + e.getMessage());
    }
}

Path gitAttributesPath = Paths.get(request.outputDirectory, request.artifactId, "gitattributes");
if (Files.exists(gitAttributesPath)) {
    Path targetPath = gitAttributesPath.getParent().resolve(".gitattributes");
    try {
        Files.move(gitAttributesPath, targetPath);
    } catch (Exception e) {
        System.err.println("Encountered a <" + e.getClass().getName() + "> exception when trying to post processing the <gitattributes> file to <.gitattributes>: " + e.getMessage());
    }
}
